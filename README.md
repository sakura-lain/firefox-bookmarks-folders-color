# Changing Firefox bookmarks folders color

*Tested under Firefox 66.0.5 bits on Linux Mint 19 Cinnamon.*

**Under Linux:** create a chrome folder in your `$HOME/.mozilla/firefox/your_user_profile/`. Copy the `userChrome.css` into it.

You can code your favorite color in the first function:

```
.bookmark-item[container], treechildren::-moz-tree-image(container) {
fill: darkblue !important;
}
```

Colors can be coded via their name (gold, red, darkblue, etc.) as well as thanks to their hexadecimal code.

**Before:**

![](Pictures/firefox_66_dir_default.png)

**After:**

![](Pictures/firefox_66_dir_darkblue.png)